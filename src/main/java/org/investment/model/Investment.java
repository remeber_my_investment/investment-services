package org.investment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.investment.util.CustomDateSerializer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="INVESTMENTS")
public class Investment implements Serializable{
	private static final long serialVersionUID = 6348782155923807122L;
	@Id
	@GeneratedValue
	@Column(name="INVESTMENT_ID")
	private Long investmentId;
	
	@NotNull(message="Bank Name can't be Null")
	@Column(name="BANK_NAME")
	private String bankName;
	
	@NotNull(message="Intial Amount can't be Null")
	@Column(name="INITIAL_AMOUNT")
	private Double initialAmount;
	
	@NotNull(message="Final Amount can't be Null")
	@Column(name="FINAL_AMOUNT")
	private Double finalAmount;
	
	@NotNull(message="User Id can't be Null")
	@Column(name="USER_ID")
	private Long userId;
	
	@JsonSerialize(using=CustomDateSerializer.class)
	@NotNull(message="Start Date can't be Null")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;
	
	@JsonSerialize(using=CustomDateSerializer.class)
	@NotNull(message="Maturity Date can't be Null")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yy")
	@Temporal(TemporalType.DATE)
	@Column(name="MATURITY_DATE")
	private Date maturityDate;
	
	public Long getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(Long investmentId) {
		this.investmentId = investmentId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public Double getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(Double initialAmount) {
		this.initialAmount = initialAmount;
	}
	public Double getFinalAmount() {
		return finalAmount;
	}
	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}
}
