package org.investment.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.investment.model.Investment;
import org.investment.repository.InvestmentRepository;
import org.springframework.stereotype.Service;

@Service
public class InvestmentService {
	
	private final InvestmentRepository investmentRepository;

	public InvestmentService(InvestmentRepository investmentRepository) {
		this.investmentRepository = investmentRepository;
	}

	public Investment getInvestment(Long investId) throws Exception{
		return investmentRepository.findById(investId).orElseThrow(() -> new Exception("Data for invest id not present"));
	}
	
	public Optional<Investment> addInvestment(Investment investment){
		return Optional.ofNullable(investmentRepository.save(investment));
	}
	
	public Optional<Investment> updateInvestment(Investment oldinvestment,Investment newinvestment){
		return Optional.ofNullable(investmentRepository.save(newinvestment));
	}
	
	public void deleteInvestment(Long investID){
		investmentRepository.deleteById(investID);
	}
	
	public List<Investment> getInvestmentsByUserId(Long userId){
		return Collections.EMPTY_LIST;
	}
	
}
