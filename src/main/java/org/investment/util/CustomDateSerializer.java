package org.investment.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomDateSerializer extends JsonSerializer<Date>{
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");

	@Override
	public void serialize(Date date, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		String formattedDate = dateFormat.format(date); 
		gen.writeString(formattedDate);
	}

}
