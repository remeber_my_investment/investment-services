package org.investment.controller;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.investment.model.Investment;
import org.investment.services.InvestmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/investments")
public class InvestmentController {
	
	private final InvestmentService investmentService;
	
	public InvestmentController(InvestmentService investmentService) {
		super();
		this.investmentService = investmentService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Investment> getInvestment(@PathVariable("id") String id,@CookieValue(value="id", defaultValue="no") String userID) throws Exception{
		return ResponseEntity.ok(investmentService.getInvestment(Long.valueOf(id)));
	}
	
	@PostMapping
	public ResponseEntity<?>  addInvestment(@Valid @RequestBody Investment investment,BindingResult result){
		if(result.hasErrors()) {
			List<String> errorList = result.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errorList);
		}else {
			return this.investmentService.addInvestment(investment).map(inv->{
				URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(inv.getInvestmentId()).toUri();
				return ResponseEntity.created(location).build();
			}).orElse(ResponseEntity.noContent().build());
		}
	}
	
	@PutMapping
	public  ResponseEntity<?> updateInvestment(@Valid @RequestBody Investment investment,BindingResult result) throws Exception{
		if(result.hasErrors()) {
			List<String> errorList = result.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errorList);
		}else {
			Investment oldInvestment = investmentService.getInvestment(investment.getInvestmentId());
			return investmentService.updateInvestment(oldInvestment,investment).map(t -> {
				return ResponseEntity.ok(t);
			}).orElse(ResponseEntity.noContent().build());
		}
	}
	
	@DeleteMapping("/{id}")
	public void deleteInvestment(@PathParam("id")Long investId) throws Exception{
		investmentService.deleteInvestment(investId);
	}
	
	@GetMapping
	public List<Investment> getInvestmentsByUserId(@RequestParam("userId")Long userId){
		return Collections.EMPTY_LIST;
	}
	
	public void readAllCookies(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if(cookies!=null) {
			String cookie = Arrays.stream(cookies).map(c -> c.getName() +"=" + c.getValue()).collect(Collectors.joining(", "));
		}
	}
}
