# investment-services
GET Request:
http://localhost:8082/investment-services/api/investments/11

POST Request:
http://localhost:8082/investment-services/api/investments
Body:	{
	"userId": 11,
	"bankName": "SBI",
	"initialAmount": 21000,
	"finalAmount": 21000,
	"startDate": "21-07-1997",
	"maturityDate": "22-07-2000"
}